-- This file should undo anything in `up.sql`

-- ----
-- User
-- ----

drop table users;
drop table user_friend;

-- ----
-- Role
-- ----

drop table roles;

-- -------
-- Channel
-- -------

drop table channels;
drop table channel_user;
drop table channel_user_role;

-- --------
-- Messages
-- --------

drop table messages;