-- Your SQL goes here
-- CREATE EXTENSION IF NOT EXISTS "uuid-ossp";


-- ----
-- User
-- ----

CREATE TABLE users (
  id uuid primary key,
  "name" text not null,
  tag smallint not null,
  email text not null,
  password text not null,
  is_bot bool default false not null
);

create table user_friend (
  user_id uuid not null,
  friend_id uuid not null,
  primary key (user_id, friend_id)
);

-- ----
-- Role
-- ----

create table roles (
  id uuid primary key,
  "name" text not null,
  permissions int4 default 0 not null
);

-- --------
-- Messages
-- --------

create table messages (
  id uuid primary key,
  author_id uuid not null,
  channel_id uuid not null,
  content text not null,
  "time" timestamp not null
);

-- -------
-- Channel
-- -------

CREATE TABLE channels (
  id uuid primary key,
  "name" text not null
);

create table channel_user (
  channel_id uuid not null,
  user_id uuid not null,
  primary key (channel_id, user_id)
);

create table channel_user_role (
  channel_id uuid not null,
  user_id uuid not null,
  role_id uuid not null,
  primary key (channel_id, user_id, role_id)
);

--create table channel_message (
--  channel_id uuid not null,
--  message_id uuid not null,
--  primary key (channel_id, message_id)
--);