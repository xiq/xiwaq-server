#[macro_use]
extern crate diesel;

mod actors;
mod alias;
mod config;
mod handlers;
mod identity;
mod jwt;
mod messages;
mod models;
mod prelude;
mod schema;

use diesel::pg::PgConnection;
use diesel::prelude::*;
use dotenv::dotenv;
use std::env;

use crate::actors::auth::AuthActor;
use crate::actors::db::DbExecutor;
use crate::config::app::connect_to_db;
use crate::config::{db_max_connections, db_url, jwt_decoding_key, jwt_encoding_key};
use crate::messages::{
    CreateChannel, CreateUser, GetChannels, GetMessages, GetUser, JoinToChannel, LeaveChannel,
    SendMessage,
};
use crate::models::dto::*;
use actix::{Actor, Recipient, SyncArbiter};
use actix_cors::Cors;
use actix_web::middleware::Logger;
use actix_web::web;
use actix_web::web::Data;
use actix_web::{get, post, App, HttpRequest, HttpResponse, HttpServer, Responder};
use diesel::r2d2::ConnectionManager;
use jsonwebtoken::{DecodingKey, EncodingKey};
use log::error;
use log::info;
use r2d2::Pool;
use uuid::Uuid;

pub struct ServerConf {}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();
    env::set_var("RUST_LOG", "actix_web=debug,actix_server=info");
    env_logger::init();
    //crate::config::logger::config_logger();

    let encoding_key = jwt_encoding_key();
    let decoding_key = jwt_decoding_key();
    let db_pool = connect_to_db(db_url().as_str(), db_max_connections());

    let db = SyncArbiter::start(db_pool.max_size() as usize, move || DbExecutor {
        db: db_pool.clone(),
    });

    let auth = AuthActor {
        get_user: db.clone().recipient(),
        encoding_key: encoding_key.clone(),
        decoding_key: decoding_key.clone(),
    }
    .start();

    let server = HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .wrap(Cors::default())
            .data(auth.clone())
            .data(encoding_key.clone())
            .data(decoding_key.clone())
            .app_data(encoding_key.clone())
            .app_data(decoding_key.clone())
            .data(db.clone().recipient::<GetUser>())
            .data(db.clone().recipient::<CreateUser>())
            .data(db.clone().recipient::<CreateChannel>())
            .data(db.clone().recipient::<GetChannels>())
            .data(db.clone().recipient::<JoinToChannel>())
            .data(db.clone().recipient::<LeaveChannel>())
            .data(db.clone().recipient::<SendMessage>())
            .data(db.clone().recipient::<GetMessages>())
            .configure(config_api_v0)
    })
    .bind("127.0.0.1:8088")
    .unwrap();

    server.run().await
}
use futures::future::Lazy;
fn config_api_v0(cfg: &mut web::ServiceConfig) {
    use crate::handlers::api::v0;
    cfg.service(
        web::scope("/api/v0")
            .wrap(identity::Identity::new())
            .service(v0::ping)
            .service(v0::signup)
            .service(v0::login)
            .service(v0::user_by_id)
            .service(v0::create_channel)
            .service(v0::join_to_channel)
            .service(v0::leave_from_channel)
            .service(v0::get_messages)
            .service(v0::send_message)
            .service(v0::get_user_channels),
    );
}
