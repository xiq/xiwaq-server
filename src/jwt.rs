//TokenData

//pub struct TokenEncodeKey<'a>(&'a [u8]);
//pub struct TokenDecodeKey<'a>(&'a [u8]);

pub struct LoggedInUser {}

#[derive(Debug)]
pub enum Error {
    InvalidHeader,
    InvalidToken(jsonwebtoken::errors::Error),
}

impl From<jsonwebtoken::errors::Error> for Error {
    fn from(e: jsonwebtoken::errors::Error) -> Self {
        Error::InvalidToken(e)
    }
}

// impl<T> FromRequest for TokenData<T>
// where
//     T: DeserializeOwned,
// {
//     type Error = Error;
//     type Future = Ready<Result<Self, Self::Error>>;
//     type Config = ();
//
//     fn from_request(req: &HttpRequest, payload: &mut Payload<PayloadStream>) -> Self::Future {
//         if let Some(authen_header) = req.headers().get("Authorization") {
//             if let Some(authen_str) = authen_header.to_str().ok() {
//                 if authen_str.starts_with("bearer") || authen_str.starts_with("Bearer") {
//                     let token_data = authen_str[6..authen_str.len()].trim();
//                     let key = req
//                         .app_data::<DecodingKey>()
//                         .expect("Decoding key is absent in app state.");
//                     let token = jsonwebtoken::decode(token_data, key, &Validation::default())
//                         .map_err(|e| Error::from(e));
//                     match token {
//                         Ok(t) => ok(t),
//                         Err(e) => err(e),
//                     }
//                 } else {
//                     err(Error::InvalidHeader)
//                 }
//             } else {
//                 err(Error::InvalidHeader)
//             }
//         } else {
//             err(Error::InvalidHeader)
//         }
//     }
// }
