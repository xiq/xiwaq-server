use crate::models::UserId;
use actix_service::{Service, Transform};
use actix_web::dev::{Payload, PayloadStream, ServiceRequest, ServiceResponse};
use actix_web::error::ErrorUnauthorized;
use actix_web::{Error, FromRequest, HttpMessage, HttpRequest};
use futures::future::{err, lazy, ok, ready, Lazy, Ready};
use jsonwebtoken::{DecodingKey, EncodingKey, TokenData, Validation};
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::sync::Arc;
use std::task::{Context, Poll};
use uuid::Uuid;

#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct UserToken {
    pub id: UserId,
}

impl FromRequest for UserToken {
    type Error = Error;
    type Future = Ready<Result<Self, Self::Error>>;
    type Config = ();

    fn from_request(req: &HttpRequest, payload: &mut Payload<PayloadStream>) -> Self::Future {
        if let Some(token) = req.extensions().get::<UserToken>() {
            ok(token.clone())
        } else {
            err(ErrorUnauthorized(""))
        }
    }
}

pub struct IdentityConfig<'a> {
    pub encoding_key: EncodingKey,
    pub decoding_key: DecodingKey<'a>,
}

// --------
// Identity
// --------

pub struct Identity {}

impl Identity {
    pub fn new() -> Self {
        Self {}
    }
}

impl<S, B> Transform<S> for Identity
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = actix_web::Error>,
    S::Future: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = S::Error;
    type Transform = IdentityMiddleware<S>;
    type InitError = ();
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ok(IdentityMiddleware { service })
    }
}

// ------------------
// IdentityMiddleware
// ------------------

pub struct IdentityMiddleware<S> {
    service: S,
}

impl<S, B> Service for IdentityMiddleware<S>
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = actix_web::Error>,
    S::Future: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = actix_web::Error;
    //type Future = Either<S::Future, Ready<Result<Self::Response, Self::Error>>>;
    //type Future = Ready<Result<Self::Response, Self::Error>>;
    type Future = S::Future;

    fn poll_ready(&mut self, cx: &mut Context) -> Poll<Result<(), Self::Error>> {
        self.service.poll_ready(cx)
    }

    fn call(&mut self, req: ServiceRequest) -> Self::Future {
        let token = parse_req::<UserToken>(&req);

        match token {
            Err(_) => self.service.call(req),
            Ok(identity) => {
                req.extensions_mut().insert(identity.claims);
                self.service.call(req)
            }
        }
    }
}

// -----
// Other
// -----

fn parse_req<T: DeserializeOwned>(req: &ServiceRequest) -> Result<TokenData<T>, ()> {
    let authen_header = req.headers().get("Authorization").ok_or_else(|| ())?;
    let mut authen_str = authen_header.to_str().map_err(|_| ())?;

    if !(authen_str.starts_with("bearer") || authen_str.starts_with("Bearer")) {
        return Err(());
    }

    let token_data = authen_str[7..].trim();

    let key = req
        .app_data::<DecodingKey>()
        .expect("Token keys is absent in app state.");

    let mut validation = Validation::default();
    // TODO: use exp
    validation.validate_exp = false;
    let token = jsonwebtoken::decode::<T>(token_data, &key, &validation);

    token.map_err(|_| ())
}
