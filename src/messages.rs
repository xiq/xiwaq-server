use crate::models::domain::*;
use crate::models::{ChannelId, Pagination, UserId};
use serde::export::Formatter;
use uuid::Uuid;

#[derive(Debug)]
pub enum Error {
    Internal,
    NotFound,
    Duplicate,
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str("Message error.");
        Ok(())
    }
}

type Result<T> = std::result::Result<T, Error>;

// ----
// User
// ----

// GetUser

pub enum GetUser {
    ById(UserId),
    ByLogin { login: String, password: String },
}

impl GetUser {
    pub fn by_id(id: UserId) -> Self {
        GetUser::ById(id)
    }
    pub fn by_uuid(id: Uuid) -> Self {
        GetUser::ById(UserId(id))
    }
    pub fn by_login(login: String, password: String) -> Self {
        GetUser::ByLogin { login, password }
    }
}

impl actix::Message for GetUser {
    type Result = Result<User>;
}

// CreateUser

pub struct CreateUser {
    pub email: String,
    pub name: String,
    pub password: String,
    pub is_bot: bool,
}

impl actix::Message for CreateUser {
    type Result = Result<User>;
}

// -------
// Channel
// -------

// CreateChannel

pub struct CreateChannel {
    pub name: String,
}

impl actix::Message for CreateChannel {
    type Result = Result<Channel>;
}

// GetChannel

pub enum GetChannels {
    WithUser(UserId),
}

impl actix::Message for GetChannels {
    type Result = Result<Vec<Channel>>;
}

// JoinToChannel

pub struct JoinToChannel {
    pub user_id: UserId,
    pub channel_id: ChannelId,
}

impl actix::Message for JoinToChannel {
    type Result = Result<()>;
}

// LeaveChannel

pub struct LeaveChannel {
    pub user_id: UserId,
    pub channel_id: ChannelId,
}

impl actix::Message for LeaveChannel {
    type Result = Result<()>;
}

// --------
// Messages
// --------

// SendMessage

pub enum SendMessageError {
    Internal,
    ChannelNotFound,
    NoPermissions,
}

pub struct SendMessage {
    pub msg: NewMessage,
    pub channel_id: ChannelId,
}

impl actix::Message for SendMessage {
    type Result = std::result::Result<(), SendMessageError>;
}

// GetMessages

pub struct GetMessages {
    pub channel_id: ChannelId,
    pub pagination: Pagination,
}

impl actix::Message for GetMessages {
    type Result = Result<Vec<Message>>;
}
