use crate::alias::DbPool;
use crate::identity::UserToken;
use crate::messages::GetUser;
use crate::models::domain::User;
use actix::{Actor, Context, Handler, Message, Recipient};
use jsonwebtoken::{DecodingKey, EncodingKey, TokenData};

pub struct AuthActor {
    pub get_user: Recipient<GetUser>,
    pub encoding_key: EncodingKey,
    pub decoding_key: DecodingKey<'static>,
}

impl Actor for AuthActor {
    type Context = Context<Self>;
}

impl AuthActor {
    // pub fn new(db: DbPool, encoding_key: EncodingKey, decoding_key: DecodingKey<'static>) -> Self {
    //     Self {
    //         db,
    //         encoding_key,
    //         decoding_key,
    //     }
    // }
}

impl Handler<Login> for AuthActor {
    type Result = <Login as Message>::Result;

    fn handle(&mut self, msg: Login, ctx: &mut Self::Context) -> Self::Result {
        let Login { email, password } = msg;

        unimplemented!()
    }
}

impl Handler<Signup> for AuthActor {
    type Result = <Signup as Message>::Result;

    fn handle(&mut self, msg: Signup, ctx: &mut Self::Context) -> Self::Result {
        unimplemented!()
    }
}

// --------
// Messages
// --------

// Login

pub struct Login {
    email: String,
    password: String,
}

impl Message for Login {
    type Result = Result<(TokenData<UserToken>, User), ()>;
}

// Signup

pub struct Signup {
    email: String,
    name: String,
    password: String,
}

impl Message for Signup {
    type Result = Result<User, ()>;
}
