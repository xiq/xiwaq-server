use crate::alias::DbPool;
use crate::messages::*;
use crate::models::dao::*;
use crate::models::domain::{Channel, Message, NewMessage};
use crate::models::{ChannelId, Pagination};
use actix::Message as AMessage;
use actix::{Actor, Handler, SyncContext};
use chrono::{NaiveDateTime, Utc};
use diesel::prelude::*;
use rand::*;
use uuid::Uuid;

pub struct DbExecutor {
    pub db: DbPool,
}

impl Actor for DbExecutor {
    type Context = SyncContext<Self>;
}

impl From<diesel::result::Error> for crate::messages::Error {
    fn from(e: diesel::result::Error) -> Self {
        use crate::messages;
        use diesel::result;
        match e {
            result::Error::NotFound => messages::Error::NotFound,
            _ => messages::Error::Internal,
        }
    }
}

// GetUser impl

impl Handler<GetUser> for DbExecutor {
    type Result = <GetUser as actix::Message>::Result;

    fn handle(&mut self, msg: GetUser, _: &mut Self::Context) -> Self::Result {
        use crate::schema::users::dsl;

        let connection = &self.db.get().unwrap();
        let user: UserDao = match msg {
            GetUser::ById(user_id) => dsl::users
                .find(user_id.0)
                .get_result::<UserDao>(connection)?,
            GetUser::ByLogin { login, password } => dsl::users
                .filter(dsl::email.eq(&login).and(dsl::password.eq(&password)))
                .first::<UserDao>(connection)?,
        };

        Ok(user.into())
    }
}

// CreateUser impl

impl Handler<CreateUser> for DbExecutor {
    type Result = <CreateUser as actix::Message>::Result;

    fn handle(&mut self, msg: CreateUser, ctx: &mut Self::Context) -> Self::Result {
        use crate::schema::users::dsl;
        let db = &self.db.get().unwrap();

        let CreateUser {
            email,
            name,
            password,
            is_bot,
        } = msg;

        // TODO: Заменить на индексацию + форматирование ошибки sql
        let count: i64 = dsl::users
            .filter(dsl::email.eq(&email))
            .count()
            .get_result(db)?;
        if count != 0 {
            return Err(crate::messages::Error::Duplicate);
        }

        // TODO: validate tag

        let user_uuid = uuid::Uuid::new_v4();
        let user_tag = rand::thread_rng().gen_range(0, std::i16::MAX);

        let new_user = NewUserDao {
            id: &user_uuid,
            name: name.as_str(),
            email: email.as_str(),
            tag: user_tag,
            password: password.as_str(),
            is_bot: is_bot,
        };

        diesel::insert_into(dsl::users)
            .values(&new_user)
            .execute(db)?;

        let user: UserDao = dsl::users.find(&user_uuid).first::<UserDao>(db)?;
        Ok(user.into())
    }
}
// ---------------
// Channel's impls
// ---------------

// CreateChannel impl

impl Handler<CreateChannel> for DbExecutor {
    type Result = <CreateChannel as actix::Message>::Result;

    fn handle(&mut self, msg: CreateChannel, ctx: &mut Self::Context) -> Self::Result {
        let db = self
            .db
            .get()
            .map_err(|_| crate::messages::Error::Internal)?;

        let CreateChannel { name } = msg;

        let channel = ChannelDao {
            name,
            id: Uuid::new_v4(),
        };

        use crate::schema::channels::dsl as c;
        let _result = diesel::insert_into(c::channels)
            .values(&channel)
            .execute(&db)?;

        Ok(channel.into())
    }
}

// GetChannels impl

impl Handler<GetChannels> for DbExecutor {
    type Result = <GetChannels as actix::Message>::Result;

    fn handle(&mut self, msg: GetChannels, ctx: &mut Self::Context) -> Self::Result {
        let db = self.db.get().unwrap();
        match msg {
            GetChannels::WithUser(user_id) => {
                use crate::schema::channel_user::dsl as cu;
                use crate::schema::channels::dsl as ch;
                let result = ch::channels
                    .inner_join(cu::channel_user.on(ch::id.eq(cu::channel_id)))
                    .filter(cu::user_id.eq(user_id.0))
                    .load::<(ChannelDao, ChannelUserDao)>(&db)?;

                Ok(result
                    .into_iter()
                    .map(|(ch, _)| Channel::from(ch))
                    .collect())
            }
        }
    }
}

// JoinToChannel impl
impl Handler<JoinToChannel> for DbExecutor {
    type Result = <JoinToChannel as actix::Message>::Result;

    fn handle(&mut self, msg: JoinToChannel, ctx: &mut Self::Context) -> Self::Result {
        let db = self.db.get().unwrap();
        let JoinToChannel {
            user_id,
            channel_id,
        } = msg;
        use crate::schema::channel_user::dsl as cu;

        let channel_user = ChannelUserDao {
            channel_id: channel_id.0,
            user_id: user_id.0,
        };

        diesel::insert_into(cu::channel_user)
            .values(&channel_user)
            .execute(&db)?;
        Ok(())
    }
}

// LeaveChannel impl
impl Handler<LeaveChannel> for DbExecutor {
    type Result = <LeaveChannel as actix::Message>::Result;

    fn handle(&mut self, msg: LeaveChannel, ctx: &mut Self::Context) -> Self::Result {
        let db = self.db.get().unwrap();
        let LeaveChannel {
            user_id,
            channel_id,
        } = msg;
        use crate::schema::channel_user::dsl as cu;

        diesel::delete(cu::channel_user)
            .filter(
                cu::user_id
                    .eq(&user_id.0)
                    .and(cu::channel_id.eq(&channel_id.0)),
            )
            .execute(&db)?;
        Ok(())
    }
}

// -------------
// Message impl
// -------------

// SendMessage impl
impl Handler<SendMessage> for DbExecutor {
    type Result = <SendMessage as actix::Message>::Result;

    fn handle(&mut self, msg: SendMessage, ctx: &mut Self::Context) -> Self::Result {
        use crate::schema::channel_user::dsl as cu;
        use crate::schema::messages::dsl as m;

        let db = self.db.get().map_err(|_| SendMessageError::Internal)?;

        let SendMessage {
            msg: NewMessage { content, author },
            channel_id,
        } = msg;

        let this_user_on_channel = cu::channel_user
            .filter(
                cu::channel_id
                    .eq(&channel_id.0)
                    .and(cu::user_id.eq(&author.0)),
            )
            .count()
            .get_result(&db)
            .map_err(|_| SendMessageError::Internal)?;

        match this_user_on_channel {
            0 => return Err(SendMessageError::NoPermissions),
            n if n > 1 => {
                log::error!(
                    "Illegal state of DB. Duplicate user on channel. Expected 0 or 1, found {}",
                    n
                );
                return Err(SendMessageError::Internal);
            }
            _ => (),
        }

        let message_dao = MessageDao {
            id: Uuid::new_v4(),
            channel_id: channel_id.0,
            author_id: author.0,
            content,
            time: Utc::now().naive_utc(),
        };

        let _ = diesel::insert_into(m::messages)
            .values(&message_dao)
            .execute(&db)
            .map_err(|_| SendMessageError::Internal)?;

        Ok(())
    }
}

// GetMessages impl
impl Handler<GetMessages> for DbExecutor {
    type Result = <GetMessages as actix::Message>::Result;

    fn handle(&mut self, msg: GetMessages, ctx: &mut Self::Context) -> Self::Result {
        let db = self.db.get().unwrap();
        let GetMessages {
            channel_id,
            pagination: Pagination { limit, offset },
        } = msg;

        use crate::schema::messages::dsl as m;
        use crate::schema::users::dsl as u;

        let message_user_vec: Vec<(MessageDao, UserDao)> = m::messages
            .inner_join(u::users.on(m::author_id.eq(u::id)))
            .filter(m::channel_id.eq(channel_id.0))
            .offset(offset as i64)
            .limit(limit as i64)
            .order(m::time.desc())
            .load::<(MessageDao, UserDao)>(&db)?;

        Ok(message_user_vec.into_iter().map(|el| el.into()).collect())
    }
}
