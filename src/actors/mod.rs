use actix::MailboxError;

pub mod auth;
pub mod db;
pub mod user;

// impl From<MailboxError> for actix_web::error::Error {
//     fn from(e: MailboxError) -> Self {
//         actix_web::error::ErrorInternalServerError("")
//     }
// }
