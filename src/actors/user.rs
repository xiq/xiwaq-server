//
// pub enum Error {
//     Query(diesel::result::Error),
// }
//
// impl From<diesel::result::Error> for Error {
//     fn from(e: diesel::result::Error) -> Self {
//         Error::Query(e)
//     }
// }
//
// type Result<T> = std::result::Result<T, Error>;
//
// //type AMessage = actix::Message;
//
// pub struct UserActor {
//     db: DbPool,
// }
//
// impl UserActor {
//     pub fn new(db: DbPool) -> Self {
//         Self { db }
//     }
// }
//
// impl Actor for UserActor {
//     type Context = Context<Self>;
// }
//
// // GetUser impl
//
// impl Handler<GetUser> for UserActor {
//     type Result = <GetUser as actix::Message>::Result;
//
//     fn handle(&mut self, msg: GetUser, _: &mut Self::Context) -> Self::Result {
//         use crate::schema::users::dsl;
//
//         let connection = &self.db.get().unwrap();
//         let result: Option<UserDao> = match msg {
//             GetUser::ById(user_id) => dsl::users
//                 .find(user_id.0)
//                 .get_result::<UserDao>(connection)
//                 .optional()?,
//             GetUser::ByLoginInfo(UserLoginInfo { email, password }) => dsl::users
//                 .filter(dsl::email.eq(&email).and(dsl::password.eq(&password)))
//                 .first::<UserDao>(connection)
//                 .optional()?,
//         };
//
//         match result {
//             Some(user) => Ok(Some(user.into())),
//             None => Ok(None),
//         }
//     }
// }
//
// // CreateUser impl
//
// impl Handler<CreateUser> for UserActor {
//     type Result = <CreateUser as actix::Message>::Result;
//
//     fn handle(&mut self, msg: CreateUser, ctx: &mut Self::Context) -> Self::Result {
//         use crate::schema::users::dsl;
//         let db = &self.db.get().unwrap();
//
//         let CreateUser {
//             email,
//             name,
//             password,
//         } = msg;
//
//         // TODO: Заменить на индексацию + форматирование ошибки sql
//         let user_list: Vec<UserDao> = dsl::users
//             .filter(dsl::email.eq(&email))
//             .load::<UserDao>(db)
//             .map_err(|_| ())?;
//         if user_list.len() != 0 {
//             return Err(());
//         }
//
//         // TODO: validate tag
//
//         let user_uuid = uuid::Uuid::new_v4();
//         let user_tag = rand::thread_rng().gen_range(0, 16000);
//
//         let new_user = NewUserDao {
//             id: &user_uuid,
//             name: name.as_str(),
//             email: email.as_str(),
//             tag: user_tag,
//             password: password.as_str(),
//             is_bot: false,
//         };
//
//         diesel::insert_into(dsl::users)
//             .values(&new_user)
//             .execute(db)
//             .map_err(|_| ())?;
//
//         let user = dsl::users
//             .find(&user_uuid)
//             .first::<UserDao>(db)
//             .map_err(|_| ())?;
//         Ok(user.into())
//     }
// }
//
// // --------
// // Messages
// // --------
//
// // GetUser
//
// pub enum GetUser {
//     ById(UserId),
//     ByLoginInfo(UserLoginInfo),
// }
//
// impl GetUser {
//     // pub fn by_email(email: String) -> Self {
//     //     GetUser::ByEMail(email)
//     // }
//     // pub fn by_name(name: String, tag: UserTag) -> Self {
//     //     GetUser::ByName(name, tag)
//     // }
//     pub fn by_id(id: UserId) -> Self {
//         GetUser::ById(id)
//     }
//     pub fn by_email_checked(email: String, password: String) -> Self {
//         GetUser::ByLoginInfo(UserLoginInfo { email, password })
//     }
// }
//
// impl actix::Message for GetUser {
//     type Result = Result<Option<User>>;
// }
//
// // CreateUser
//
// pub struct CreateUser {
//     pub email: String,
//     pub name: String,
//     pub password: String,
// }
//
// impl actix::Message for CreateUser {
//     type Result = std::result::Result<User, ()>;
// }
