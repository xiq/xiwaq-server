use diesel::r2d2::ConnectionManager;
use diesel::PgConnection;
use r2d2::Pool;

pub type DbPool = Pool<ConnectionManager<PgConnection>>;
