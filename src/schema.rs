table! {
    channel_user (channel_id, user_id) {
        channel_id -> Uuid,
        user_id -> Uuid,
    }
}

table! {
    channel_user_role (channel_id, user_id, role_id) {
        channel_id -> Uuid,
        user_id -> Uuid,
        role_id -> Uuid,
    }
}

table! {
    channels (id) {
        id -> Uuid,
        name -> Text,
    }
}

table! {
    messages (id) {
        id -> Uuid,
        author_id -> Uuid,
        channel_id -> Uuid,
        content -> Text,
        time -> Timestamp,
    }
}

table! {
    roles (id) {
        id -> Uuid,
        name -> Text,
        permissions -> Int4,
    }
}

table! {
    user_friend (user_id, friend_id) {
        user_id -> Uuid,
        friend_id -> Uuid,
    }
}

table! {
    users (id) {
        id -> Uuid,
        name -> Text,
        tag -> Int2,
        email -> Text,
        password -> Text,
        is_bot -> Bool,
    }
}

allow_tables_to_appear_in_same_query!(
    channel_user,
    channel_user_role,
    channels,
    messages,
    roles,
    user_friend,
    users,
);
