use actix_web::dev::PayloadStream;
use actix_web::{FromRequest, HttpRequest};
use futures::future::{ok, Ready};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

pub mod dao;
pub mod domain;
pub mod dto;

pub type Id = Uuid;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct UserId(pub Id);

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ChannelId(pub Id);

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct RoleId(pub Id);

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MessageId(pub Id);

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub struct Pagination {
    pub offset: u32,
    pub limit: u32,
}
