use crate::models::domain::{Channel, Message, User, UserTag};
use crate::models::*;
use chrono::{DateTime, NaiveDateTime};
use serde::export::TryFrom;
use serde::*;
use std::convert::TryInto;
use uuid::Uuid;

pub enum Error {
    Uuid(uuid::Error),
    ParseInt(std::num::ParseIntError),
    ParseDateTime(chrono::ParseError),
}

impl From<uuid::Error> for Error {
    fn from(e: uuid::Error) -> Self {
        Error::Uuid(e)
    }
}

impl From<uuid::parser::ParseError> for Error {
    fn from(e: uuid::parser::ParseError) -> Self {
        Error::Uuid(uuid::Error::Parse(e))
    }
}

impl From<std::num::ParseIntError> for Error {
    fn from(e: std::num::ParseIntError) -> Self {
        Error::ParseInt(e)
    }
}

impl From<chrono::ParseError> for Error {
    fn from(e: chrono::ParseError) -> Self {
        Error::ParseDateTime(e)
    }
}

//type Result<T> = std::result::Result<T, Error>;

// -------------
// Utiliti types
// -------------

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct PaginationDto {
    pub offset: Option<u32>,
    pub limit: Option<u32>,
}

// ----
// User
// ----

// User

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct UserDto {
    pub id: String,
    pub name: String,
    pub tag: String,
    pub is_bot: bool,
}

impl From<User> for UserDto {
    fn from(u: User) -> Self {
        Self {
            id: u.id.0.to_string(),
            name: u.name,
            tag: format!("{:04x}", u.tag.0),
            is_bot: u.is_bot,
        }
    }
}

impl TryFrom<UserDto> for User {
    type Error = Error;

    fn try_from(u: UserDto) -> Result<Self, Self::Error> {
        let uuid = Uuid::parse_str(&u.id)?;
        Ok(Self {
            id: UserId(Uuid::parse_str(&u.id)?),
            name: u.name,
            tag: UserTag::try_from(u.tag.as_str())?,
            is_bot: u.is_bot,
        })
    }
}

// NewUser

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct NewUserDto {
    pub email: String,
    pub name: String,
    pub password: String,
}

// UserLoginInfo

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct UserLoginInfoDto {
    pub login: String,
    pub password: String,
}

// --------
// Channels
// --------

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct NewChannelDto {
    pub name: String,
}

// Channel dto for user
#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ChannelPreviewDto {
    pub id: ChannelId,
    pub name: String,
    pub unread_messages_count: i32,
}

impl From<Channel> for ChannelPreviewDto {
    fn from(ch: Channel) -> Self {
        let Channel { id, name } = ch;

        ChannelPreviewDto {
            id,
            name,
            unread_messages_count: 0,
        }
    }
}

// -------
// Message
// -------

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct MessageDto {
    pub id: String,
    pub content: String,
    pub author: UserDto,
    pub timestamp: String,
}

impl From<Message> for MessageDto {
    fn from(msg: Message) -> Self {
        Self {
            id: msg.id.0.to_string(),
            content: msg.content,
            author: msg.author.into(),
            timestamp: msg.time.to_string(),
        }
    }
}

impl TryFrom<MessageDto> for Message {
    type Error = Error;

    fn try_from(msg: MessageDto) -> Result<Self, Self::Error> {
        Ok(Self {
            id: MessageId(Uuid::parse_str(&msg.id)?),
            content: msg.content,
            author: msg.author.try_into()?,
            time: DateTime::parse_from_rfc3339(&msg.timestamp)?.naive_utc(),
        })
    }
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct NewMessageDto {
    pub content: String,
}
