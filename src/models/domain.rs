use crate::models::*;
use chrono::{DateTime, NaiveDateTime, Utc};
use serde::export::fmt::Error;
use serde::export::Formatter;
use serde::{Deserialize, Serialize, Serializer};
use std::convert::TryFrom;
use uuid::Uuid;

pub(crate) type Vec<T> = std::vec::Vec<T>;

// ----
// User
// ----

#[derive(Deserialize)]
#[serde(try_from = "String")]
pub struct UserTag(pub u16);

impl Serialize for UserTag {
    fn serialize<S: Serializer>(&self, s: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let str = format!("{:04x}", self.0);
        s.serialize_str(&str)
    }
}

impl std::fmt::Display for UserTag {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let str = format!("{:04x}", self.0);
        f.write_str(&str)
    }
}

impl TryFrom<String> for UserTag {
    type Error = std::num::ParseIntError;

    fn try_from(s: String) -> Result<Self, Self::Error> {
        let num = u16::from_str_radix(&s, 16)?;
        Ok(Self(num))
    }
}

impl TryFrom<&str> for UserTag {
    type Error = std::num::ParseIntError;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let num = u16::from_str_radix(s, 16)?;
        Ok(Self(num))
    }
}

pub struct User {
    pub id: UserId,
    pub name: String,
    pub tag: UserTag,
    pub is_bot: bool,
}

pub struct NewUser {
    pub email: String,
    pub name: String,
    pub password: String,
    pub is_bot: bool,
}

pub struct UserLoginInfo {
    pub email: String,
    pub password: String,
}

pub enum UserLogin {
    Login(String),
    NameTag(String, UserTag),
}

// pub struct UserPassword(String);
// pub struct UserEMail(String);

// -----
// Roles
// -----

pub struct Role {
    pub id: RoleId,
    pub name: String,
    pub permissions: i32,
}

// -------
// Message
// -------

pub struct Message {
    pub id: MessageId,
    pub content: String,
    pub author: User,
    pub time: NaiveDateTime,
}

pub struct NewMessage {
    pub content: String,
    pub author: UserId,
}

// -------
// Channel
// -------

pub struct Channel {
    pub id: ChannelId,
    pub name: String,
}

pub struct ChannelUser {
    user_id: UserId,
    role_ids: Vec<RoleId>,
}
