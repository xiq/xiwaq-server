use crate::models::domain::{Channel, Message, NewMessage, User, UserTag};
use crate::models::*;
use crate::schema::*;
use chrono::NaiveDateTime;
use diesel::associations::*;
use diesel::{Insertable, Queryable};
use uuid::Uuid;

// ----
// User
// ----

#[derive(Debug, Queryable, Insertable, Identifiable)]
#[table_name = "users"]
pub struct UserDao {
    pub id: Uuid,
    pub name: String,
    pub tag: i16,
    pub email: String,
    pub password: String,
    pub is_bot: bool,
}

impl From<UserDao> for domain::User {
    fn from(u: UserDao) -> Self {
        Self {
            id: UserId(u.id),
            name: u.name,
            tag: UserTag(u.tag as u16),
            is_bot: u.is_bot,
        }
    }
}

#[derive(Debug, Insertable)]
#[table_name = "users"]
pub struct NewUserDao<'a> {
    pub id: &'a Uuid,
    pub email: &'a str,
    pub name: &'a str,
    pub tag: i16,
    pub password: &'a str,
    pub is_bot: bool,
}

#[derive(Debug, Queryable, Insertable)]
#[table_name = "user_friend"]
pub struct UserFriendDao {
    pub user_id: Uuid,
    pub friend_id: Uuid,
}

// ----
// Role
// ----

#[derive(Debug, Queryable, Insertable)]
#[table_name = "roles"]
pub struct RoleDao {
    pub id: Uuid,
    pub name: String,
    pub permissions: i32,
}

// -------
// Channel
// -------

#[derive(Identifiable, Debug, Queryable, Insertable)]
//#[belongs_to(ChannelUserDto, foreign_key = "channel_id")]
#[table_name = "channels"]
pub struct ChannelDao {
    pub id: Uuid,
    pub name: String,
}

impl From<ChannelDao> for Channel {
    fn from(ch: ChannelDao) -> Self {
        Self {
            id: ChannelId(ch.id),
            name: ch.name,
        }
    }
}

#[derive(Debug, Insertable)]
#[table_name = "channels"]
pub struct NewChannelDao<'a> {
    pub id: &'a Uuid,
    pub name: &'a str,
}

#[derive(Identifiable, Debug, Queryable, Insertable)]
#[primary_key(channel_id, user_id)]
#[table_name = "channel_user"]
pub struct ChannelUserDao {
    pub channel_id: Uuid,
    pub user_id: Uuid,
}

#[derive(Debug, Queryable, Insertable)]
#[table_name = "channel_user_role"]
pub struct ChannelUserRoleDao {
    pub channel_id: Uuid,
    pub user_id: Uuid,
    pub role_id: Uuid,
}

// -------
// Message
// -------

#[derive(Debug, Queryable, Insertable)]
#[table_name = "messages"]
pub struct MessageDao {
    pub id: Uuid,
    pub channel_id: Uuid,
    pub author_id: Uuid,
    pub content: String,
    pub time: NaiveDateTime,
}

impl From<(MessageDao, UserDao)> for Message {
    fn from((m_dao, u_dao): (MessageDao, UserDao)) -> Self {
        let message = Message {
            id: MessageId(m_dao.id),
            content: m_dao.content,
            author: u_dao.into(),
            time: m_dao.time,
        };

        message
    }
}

#[derive(Debug, Insertable)]
#[table_name = "messages"]
pub struct NewMessageDao<'a> {
    pub id: Uuid,
    pub author_id: Uuid,
    pub channel_id: Uuid,
    pub content: &'a str,
    pub time: NaiveDateTime,
}
