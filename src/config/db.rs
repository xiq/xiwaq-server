use crate::alias::DbPool;
use crate::config::{db_max_connections, db_url};
use diesel::{
    pg::PgConnection,
    r2d2::{self, ConnectionManager},
};
use log::info;

//embed_migrations!();
pub fn migrate_and_config_db(url: &str, connections: u32) -> DbPool {
    info!("Migrating and configurating database...");
    let database_url = db_url();
    let max_connections = db_max_connections();

    let manager = ConnectionManager::<PgConnection>::new(url);
    let pool = r2d2::Pool::builder()
        .max_size(max_connections)
        .build(manager)
        .expect("Failed to create database connection pool.");
    //embedded_migrations::run(&pool.get().expect("Failed to migrate."));

    pool
}
