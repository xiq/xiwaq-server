pub mod app;
pub mod db;
pub mod logger;

use jsonwebtoken::{DecodingKey, EncodingKey};
use log::error;
use std::env;

pub fn db_url() -> String {
    env::var("DATABASE_URL").expect("No database url")
}

const DEFAULT_MAX_DB_CONNECTION_COUNT: u32 = 10;
pub fn db_max_connections() -> u32 {
    match env::var("DATABASE_CONNECTIONS").ok() {
        Some(s) => s.parse::<u32>().unwrap_or_else(|_| {
            error!("Illegal number format of DATABASE_CONNECTIONS value. Set default.");
            DEFAULT_MAX_DB_CONNECTION_COUNT
        }),
        None => DEFAULT_MAX_DB_CONNECTION_COUNT,
    }
}

pub fn jwt_decoding_key() -> DecodingKey<'static> {
    DecodingKey::from_base64_secret(
        &env::var("JWT_DECODE_KEY").expect("JWT_DECODE_KEY must be set."),
    )
    .expect("Illegal decode key.")
}

pub fn jwt_encoding_key() -> EncodingKey {
    EncodingKey::from_base64_secret(
        &env::var("JWT_ENCODE_KEY").expect("JWT_ENCODE_KEY must be set."),
    )
    .expect("Illegal encode key.")
}
