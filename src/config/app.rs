use crate::actors::auth::AuthActor;
use crate::actors::db::DbExecutor;
use crate::alias::DbPool;
use crate::config::{db_max_connections, db_url, jwt_decoding_key, jwt_encoding_key};
use actix::{Actor, SyncArbiter};
use actix_web::web;
use diesel::r2d2::ConnectionManager;
use log::*;

pub fn config_app(cfg: &mut web::ServiceConfig) {
    config_rotes(cfg);
}

fn init_server() {
    let encoding_key = jwt_encoding_key();
    let decoding_key = jwt_decoding_key();
    let db_pool = connect_to_db(db_url().as_str(), db_max_connections());

    let db = SyncArbiter::start(db_pool.max_size() as usize, move || DbExecutor {
        db: db_pool.clone(),
    });

    let auth = AuthActor {
        get_user: db.clone().recipient(),
        encoding_key: encoding_key,
        decoding_key: decoding_key,
    }
    .start();
}

pub fn connect_to_db(url: &str, connections_limit: u32) -> DbPool {
    let manager = ConnectionManager::new(url);
    let pool = r2d2::Pool::builder()
        .max_size(connections_limit)
        .build(manager)
        .expect("Failed to create db pool.");
    pool
}

fn config_rotes(cfg: &mut web::ServiceConfig) {
    info!("Configurating routes...");

    unimplemented!();
    // cfg.service(
    //
    // );
}
