use crate::actors::auth::AuthActor;
use crate::identity::UserToken;
use crate::messages;
use crate::messages::{
    CreateChannel, CreateUser, GetChannels, GetMessages, GetUser, JoinToChannel, LeaveChannel,
    SendMessage,
};
use crate::models::domain::*;
use crate::models::dto::*;
use crate::models::{ChannelId, Pagination};
use actix::{MailboxError, Recipient};
use actix_web::body::Body;
use actix_web::error::ErrorInternalServerError;
use actix_web::http::StatusCode;
use actix_web::web::{Data, Json, Path, Query};
use actix_web::{get, post, HttpRequest, HttpResponse, Responder, ResponseError};
use jsonwebtoken::{DecodingKey, EncodingKey, Header};
use log::{error, info};
use serde::export::Formatter;
use serde::Serialize;
use uuid::Uuid;
//
// #[derive(Debug)]
// enum Error<T>
// where
//     T: ResponseError,
// {
//     MailboxError,
//     ResponseError(T),
// }
//
// impl<T> std::fmt::Display for Error<T>
// where
//     T: ResponseError,
// {
//     fn fmt(&self, f: &mut Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
//         write!(f, "Error")
//     }
// }
//
// impl<T> ResponseError for Error<T>
// where
//     T: ResponseError,
// {
//     fn status_code(&self) -> StatusCode {
//         match self {
//             Error::MailboxError => StatusCode::INTERNAL_SERVER_ERROR,
//             Error::ResponseError(err) => err.status_code(),
//         }
//     }
// }
//
// impl<T> From<MailboxError> for Error<T>
// where
//     T: ResponseError,
// {
//     fn from(_: MailboxError) -> Self {
//         Error::MailboxError
//     }
// }
//
// type Result<T, E> = std::result::Result<T, Error<E>>;

#[get("/users/{id}")]
pub async fn user_by_id(
    _req: HttpRequest,
    id: Path<Uuid>,
    get_user: Data<Recipient<GetUser>>,
) -> impl Responder {
    let result = get_user.send(GetUser::by_uuid(id.into_inner())).await;
    match result {
        Ok(Ok(u)) => HttpResponse::Ok().json(UserDto::from(u)),
        Ok(Err(messages::Error::NotFound)) => HttpResponse::NotFound().finish(),
        Ok(Err(e)) => {
            error!("{}", e);
            HttpResponse::InternalServerError().finish()
        }
        Err(e) => {
            error!("{}", e);
            HttpResponse::InternalServerError().finish()
        }
    }
}

#[get("/channels")]
pub async fn get_user_channels(
    req: HttpRequest,
    token: UserToken,
    get_channels: Data<Recipient<GetChannels>>,
) -> impl Responder {
    let get_channel_cmd = GetChannels::WithUser(token.id);
    let channels = get_channels.send(get_channel_cmd).await;
    match channels {
        Ok(Ok(channels)) => HttpResponse::Ok().json(
            channels
                .into_iter()
                .map(|ch| {
                    let Channel { id, name } = ch;
                    ChannelPreviewDto {
                        id,
                        name,
                        unread_messages_count: 0,
                    }
                })
                .collect::<Vec<_>>(),
        ),
        Ok(Err(messages::Error::NotFound)) => HttpResponse::NotFound().finish(),
        Ok(Err(e)) => {
            error!("{}", e);
            HttpResponse::InternalServerError().finish()
        }
        Err(e) => {
            error!("{}", e);
            HttpResponse::InternalServerError().finish()
        }
    }
}

#[get("/ping")]
pub async fn ping(req: HttpRequest) -> impl Responder {
    HttpResponse::Ok().body(
        r###"
        Pong!
        "###,
    )
}

#[post("/auth/signup")]
pub async fn signup(
    req: HttpRequest,
    user: Json<NewUserDto>,
    create_user: Data<Recipient<CreateUser>>,
) -> impl Responder {
    let user = user.0;
    let msg = CreateUser {
        email: user.email,
        name: user.name,
        password: user.password,
        is_bot: false,
    };

    let result = create_user.send(msg).await;
    match result {
        Ok(Ok(u)) => HttpResponse::Ok().json(UserDto::from(u)),
        Ok(Err(messages::Error::Duplicate)) => HttpResponse::BadRequest().finish(),
        Ok(Err(e)) => {
            error!("{}", e);
            HttpResponse::InternalServerError().finish()
        }
        Err(e) => {
            error!("{}", e);
            HttpResponse::InternalServerError().finish()
        }
    }
}

#[derive(Serialize)]
struct UserLoginResponse {
    token: String,
    user: UserDto,
}

#[post("/auth/login")]
pub async fn login(
    req: HttpRequest,
    login_info: Json<UserLoginInfoDto>,
    get_user: Data<Recipient<GetUser>>,
    encoding_key: Data<EncodingKey>,
) -> impl Responder {
    let UserLoginInfoDto { login, password } = login_info.into_inner();
    let msg = GetUser::by_login(login, password);

    let user = get_user.send(msg);

    let result = user.await;
    match result {
        Ok(Ok(u)) => {
            // TODO: replace to AuthActor
            let user_token = UserToken { id: u.id.clone() };
            let user = UserDto::from(u);
            let token = jsonwebtoken::encode(&Header::default(), &user_token, &encoding_key);
            match token {
                Ok(token) => HttpResponse::Ok().json(UserLoginResponse { user, token }),
                Err(_) => HttpResponse::InternalServerError().finish(),
            }
        }
        Ok(Err(messages::Error::NotFound)) => HttpResponse::BadRequest().finish(),
        Ok(Err(e)) => {
            error!("{}", e);
            HttpResponse::InternalServerError().finish()
        }
        Err(e) => {
            error!("{}", e);
            HttpResponse::InternalServerError().finish()
        }
    }
}

// -------
// Channel
// -------

#[post("/channels/create")]
pub async fn create_channel(
    req: HttpRequest,
    token: UserToken,
    channel_dto: Json<NewChannelDto>,
    create_channel: Data<Recipient<CreateChannel>>,
) -> Result<impl Responder, actix_web::error::Error> {
    let msg = CreateChannel {
        name: channel_dto.into_inner().name,
    };

    let result = create_channel
        .send(msg)
        .await
        .map_err(|_| ErrorInternalServerError(""))?;

    match result {
        Ok(ch) => Ok(HttpResponse::Ok().json(ChannelPreviewDto::from(ch))),
        Err(_) => Ok(HttpResponse::BadRequest().finish()),
    }
}

#[post("/channels/{channelId}/join")]
pub async fn join_to_channel(
    req: HttpRequest,
    id: Path<Uuid>,
    token: UserToken,
    join_to_channel: Data<Recipient<JoinToChannel>>,
) -> Result<impl Responder, actix_web::error::Error> {
    let channel_id = ChannelId(id.into_inner());
    let user_id = token.id;
    let msg = JoinToChannel {
        user_id,
        channel_id,
    };

    let result = join_to_channel
        .send(msg)
        .await
        .map_err(|_| ErrorInternalServerError(""))?;

    match result {
        Ok(_) => Ok(HttpResponse::Ok().finish()),
        Err(_) => Ok(HttpResponse::BadRequest().finish()),
    }
}

#[post("/channels/{channelId}/leave")]
pub async fn leave_from_channel(
    req: HttpRequest,
    id: Path<Uuid>,
    token: UserToken,
    leave_from_channel: Data<Recipient<LeaveChannel>>,
) -> Result<impl Responder, actix_web::error::Error> {
    let channel_id = ChannelId(id.into_inner());
    let user_id = token.id;
    let msg = LeaveChannel {
        user_id,
        channel_id,
    };

    let result = leave_from_channel
        .send(msg)
        .await
        .map_err(|_| ErrorInternalServerError(""))?;

    match result {
        Ok(_) => Ok(HttpResponse::Ok().finish()),
        Err(_) => Ok(HttpResponse::BadRequest().finish()),
    }
}

#[post("/channels/{channelId}/send")]
pub async fn send_message(
    req: HttpRequest,
    channel_id: Path<Uuid>,
    content: Json<NewMessageDto>,
    token: UserToken,
    send_message: Data<Recipient<SendMessage>>,
) -> Result<impl Responder, actix_web::error::Error> {
    let channel_id = ChannelId(channel_id.into_inner());
    let user_id = token.id;
    let msg = SendMessage {
        msg: NewMessage {
            content: content.into_inner().content,
            author: user_id,
        },
        channel_id,
    };

    let result = send_message
        .send(msg)
        .await
        .map_err(|_| ErrorInternalServerError(""))?;

    match result {
        Ok(_) => Ok(HttpResponse::Ok().finish()),
        Err(_) => Ok(HttpResponse::BadRequest().finish()),
    }
}

#[get("/channels/{channelId}/messages")]
pub async fn get_messages(
    req: HttpRequest,
    channel_id: Path<Uuid>,
    pagination: Query<PaginationDto>,
    token: UserToken,
    get_messages: Data<Recipient<GetMessages>>,
) -> Result<impl Responder, actix_web::error::Error> {
    const DEFAULT_OFFSET: u32 = 0;
    const DEFAULT_LIMIT: u32 = 50;

    let pagination = Pagination {
        offset: pagination.offset.unwrap_or(DEFAULT_OFFSET),
        limit: pagination.limit.unwrap_or(DEFAULT_LIMIT),
    };

    let channel_id = channel_id.into_inner();

    let msg = GetMessages {
        channel_id: ChannelId(channel_id),
        pagination,
    };

    let result = get_messages
        .send(msg)
        .await
        .map_err(|_| ErrorInternalServerError(""))?;

    match result {
        Ok(msgs) => {
            let msgs: Vec<_> = msgs.into_iter().map(|msg| MessageDto::from(msg)).collect();
            Ok(HttpResponse::Ok().json(msgs))
        }
        Err(_) => Ok(HttpResponse::BadRequest().finish()),
    }
}
